<?php 	
	spl_autoload_register('autoload');
	function autoload($class){
		$clase = str_replace('admin\\', '', $class);
		$clase = dirname(__FILE__) . DIRECTORY_SEPARATOR. str_replace("\\", DIRECTORY_SEPARATOR, $clase). '.php';

		if(!file_exists($clase)){
			$clase1 =  dirname(__FILE__) . DIRECTORY_SEPARATOR. str_replace("\\", DIRECTORY_SEPARATOR, $class). '.php';
			if(!file_exists($clase1)){
				throw new Exception("Error al cargar la clase ". $clase1);
			}
			else
				require_once($class);
		}
		else
			require_once($clase);
	}
	date_default_timezone_set('America/Argentina/Buenos_Aires');
?>