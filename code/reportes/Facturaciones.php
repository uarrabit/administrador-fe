<?php
	namespace code\reportes;
	use admin\MyUFrame\Clase;
	class Facturaciones extends Clase
	{
		private $razonSocial;
		private $categoria;
		private $cuit;
		private $qFacturasDia;
		private $totalFacturadoDia;
		private $facturasEmitidasMes;
		private $totalFacturadoMes;
		private $fechaUltimaFactura;
		private $semaforo;
		
		public function __construct($razonSocial, $categoria, $cuit, $qFacturasDia, $totalFacturadoDia, $facturasEmitidasMes, $totalFacturadoMes, $fechaUltimaFactura, $semaforo){
			$this->razonSocial = sprintf("%s",$razonSocial);
        	$this->categoria = sprintf("%s",$categoria);
        	$this->cuit = sprintf("%s",$cuit);
        	$this->qFacturasDia = sprintf("%s",$qFacturasDia);
        	$this->totalFacturadoDia = sprintf("%s",$totalFacturadoDia);
        	$this->facturasEmitidasMes = sprintf("%s",$facturasEmitidasMes);
        	$this->totalFacturadoMes = sprintf("%s",$totalFacturadoMes);
        	$this->fechaUltimaFactura = sprintf("%s",$fechaUltimaFactura);
        	$this->semaforo = sprintf("%s",$semaforo);
        }
		public function setRazonSocial($value){
			$this->razonSocial = sprintf("%s",$value);
		}
		public function setCategoria($value){
			$this->categoria = sprintf("%s",$value);
		}
		public function setCuit($value){
			$this->cuit = sprintf("%s",$value);
		}
		public function setQFacturasDia($value){
			$this->qFacturasDia = sprintf("%s",$value);
		}
		public function setTotalFacturadoDia($value){
			$this->totalFacturadoDia = sprintf("%s",$value);
		}
		public function setFacturasEmitidasMes($value){
			$this->facturasEmitidasMes = sprintf("%s",$value);
		}
		public function setTotalFacturadoMes($value){
			$this->totalFacturadoMes = sprintf("%s",$value);
		}
		public function setFechaUltimaFactura($value){
			$this->fechaUltimaFactura = sprintf("%s",$value);
		}
		public function setSemaforo($value){
			$this->semaforo = sprintf("%s",$value);
		}
		public function getRazonSocial(){
			return $this->razonSocial;
		}
		public function getCategoria(){
			return $this->categoria;
		}
		public function getCuit(){
			return $this->cuit;
		}
		public function getQFacturasDia(){
			return $this->qFacturasDia;
		}
		public function getTotalFacturadoDia(){
			return $this->totalFacturadoDia;
		}
		public function getFacturasEmitidasMes(){
			return $this->facturasEmitidasMes;
		}
		public function getTotalFacturadoMes(){
			return $this->totalFacturadoMes;
		}
		public function getFechaUltimaFactura(){
			return $this->fechaUltimaFactura;
		}
		public function getSemaforo(){
			return $this->semaforo;
		}
		public function getJson(){
				$vars = get_object_vars($this);
				return Self::getArray($vars);
		}
	}
?>