<?php
namespace code\reportes;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
use code\repositorios\CtrlRepositorio;

class CtrlReporte extends Controller {

	public static function enviosProgramados(){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		foreach ($repos as $key => $value) {
			$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "$value/apis/reportes/v1/enviosProgramados/?mailDestino=",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				));

				$response = curl_exec($curl);

				curl_close($curl);
			}
		return new Respuesta(1, 'se enviaron los reportes');
	}
	public static function controlFacturacion($mesAnio){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		$datos = [];
		foreach ($repos as $key => $value) {

			$config = CtrlRepositorio::leerConfiguracionCliente($value);
			$facturacionCliente = Self::facturacionCliente($value, $mesAnio);
			$dato = new Facturaciones($config['razon_social'], $config['condicionIva'], $config['cuit'], $facturacionCliente['qFacturasDia'],$facturacionCliente['totalFacturadoDia'], $facturacionCliente['facturasEmitidasMes'], $facturacionCliente['totalFacturadoMes'], $facturacionCliente['fechaUltimaFactura'], $facturacionCliente['semaforo']);
			array_push($datos, $dato);
		}
		return new Respuesta(1, Self::ordenarFacturacion($datos));
	}
	private static function facturacionCliente($ruta, $mesAnio){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $ruta."/apis/reportes/v1/?mesAnio=".$mesAnio,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  return new Respuesta(-1, "cURL Error #:" . $err);
		} else {
		  return json_decode($response, true)['mensaje'];
		}
	}

	private static function ordenarFacturacion($facturaciones){
		$datos = [];
		foreach ($facturaciones as $key => $value) {
			$dato = $value->getJson();
			$dato['fechaUltimaFactura'] = date_create($dato['fechaUltimaFactura']);
			$dato['fechaUltimaFactura'] = $dato['fechaUltimaFactura']->format('Y-m-d H:i:s');
			array_push($datos, $dato);	
		}
		$datos = Self::array_sort($datos, 'fechaUltimaFactura', SORT_DESC);
			
		$rta = [];
		foreach ($datos as $key => $value) {
			$value['fechaUltimaFactura'] = date_create_from_format('Y-m-d H:i:s',$value['fechaUltimaFactura']);
			$fecha = explode('-', $value['fechaUltimaFactura']->format('d-m-Y'));
			if($fecha[2] < '2019')
				$fechaUltimaFactura = '00-00-00 00:00:00';
			else
				$fechaUltimaFactura= $value['fechaUltimaFactura']->format('d-m-Y H:i:s');
			array_push($rta, new Facturaciones($value['razonSocial'], $value['categoria'], $value['cuit'], $value['qFacturasDia'],$value['totalFacturadoDia'], $value['facturasEmitidasMes'], $value['totalFacturadoMes'], $fechaUltimaFactura, $value['semaforo']));
		}
		return $rta;
	}

	public static function enviarReportesProgramados(){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		foreach ($repos as $key => $value) {
			$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "$value/apis/reportes/v1/enviosProgramados/?mailDestino=",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				));
				$response = curl_exec($curl);
				curl_close($curl);
		}
	}
	public static function ControlarFaltantes(){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		$rta = "";
		foreach ($repos as $key => $value) {
			$rta .=" se envió al repositorio: $value \n";
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "$value/apis/reportes/v1/faltantes/",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);

			curl_close($curl);
		}
		return new Respuesta(1, $rta);
	}

	public static function pruebaHilos($mesAnio){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		$datos = [];
		$pool = new PoolFacturacion(10);
		foreach ($repos as $key => $value) {
			$pool->start(new HiloFacturacion($value, $mesAnio));
		}
		//$rta = $pool->process();
		var_dump($pool);die;
	}
}
?>