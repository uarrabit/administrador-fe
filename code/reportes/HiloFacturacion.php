<?php 
namespace code\reportes;

use code\Hilos\HiloBase;

/**
 * 
 */
class HiloFacturacion extends HiloBase
{
	public $ruta;
	public $mesAnio;
	public $rta;

	function __construct($ruta,$mesAnio)
	{
		$this->ruta = $ruta;
		$this->mesAnio = $mesAnio;
		$this->rta = null;
	}
	 public function initialize() 
    {
        return true;
    }
    public function onSuccess()
    {
        return true;
    }
     public function onFailure() 
    {
        return false;
    }
	public function process(){
		$facturacionCliente = Self::facturacionCliente($this->ruta, $this->mesAnio);
		$this->rta =  new Facturaciones($config['razon_social'], $config['condicionIva'], $config['cuit'], $facturacionCliente['qFacturasDia'],$facturacionCliente['totalFacturadoDia'], $facturacionCliente['facturasEmitidasMes'], $facturacionCliente['totalFacturadoMes'], $facturacionCliente['fechaUltimaFactura'], $facturacionCliente['semaforo']);
	}

	public function ternino(){
		return (!empty($this->rta)) ? TRUE : FALSE;
	}

	private static function facturacionCliente($ruta, $mesAnio){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $ruta."/apis/reportes/v1/?mesAnio=".$mesAnio,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  return new Respuesta(-1, "cURL Error #:" . $err);
		} else {
		  return json_decode($response, true)['mensaje'];
		}
	}
}
?>