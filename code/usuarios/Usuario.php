<?php
	namespace admin\code\usuarios;
	use admin\MyUFrame\Clase;
	class Usuario extends Clase
	{
		private $id;
		private $razon_social;
		private $nombre;
		private $apellido;
		private $cuit;
		private $fk_condicion_iva;
		private $fk_perfil;
		private $ruta;
		
		public function __construct($id, $razon_social, $nombre, $apellido, $cuit, $fk_condicion_iva, $fk_perfil, $ruta){
			$this->id = sprintf("%s",$id);
        	$this->razon_social = sprintf("%s",$razon_social);
        	$this->nombre = sprintf("%s",$nombre);
        	$this->apellido = sprintf("%s",$apellido);
        	$this->cuit = sprintf("%s",$cuit);
        	$this->fk_condicion_iva = sprintf("%s",$fk_condicion_iva);
        	$this->fk_perfil = sprintf("%s",$fk_perfil);
        	$this->ruta = sprintf("%s",$ruta);
        }
		public function setId($value){
			$this->id = sprintf("%s",$value);
		}
		public function setRazon_social($value){
			$this->razon_social = sprintf("%s",$value);
		}
		public function setNombre($value){
			$this->nombre = sprintf("%s",$value);
		}
		public function setApellido($value){
			$this->apellido = sprintf("%s",$value);
		}
		public function setCuit($value){
			$this->cuit = sprintf("%s",$value);
		}
		public function setFk_condicion_iva($value){
			$this->fk_condicion_iva = sprintf("%s",$value);
		}
		public function setFk_perfil($value){
			$this->fk_perfil = sprintf("%s",$value);
		}
		public function setRuta($value){
			$this->ruta = sprintf("%s",$value);
		}
		public function getId(){
			return $this->id;
		}
		public function getRazon_social(){
			return $this->razon_social;
		}
		public function getNombre(){
			return $this->nombre;
		}
		public function getApellido(){
			return $this->apellido;
		}
		public function getCuit(){
			return $this->cuit;
		}
		public function getFk_condicion_iva(){
			return $this->fk_condicion_iva;
		}
		public function getFk_perfil(){
			return $this->fk_perfil;
		}
		public function getRuta(){
			return $this->ruta;
		}
		public function getJson(){
				$vars = get_object_vars($this);
				return Self::getArray($vars);
		}
	}
?>