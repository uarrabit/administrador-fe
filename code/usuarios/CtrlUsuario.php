<?php
namespace admin\code\usuarios;
use admin\MyUFrame\Auth\JWT;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Respuesta;
use code\repositorios\v2\CtrlRepositorio;
class CtrlUsuario extends Controller {
	public function usuario(){
		$rta = DB::usuario();
		$combos = [];
		foreach($rta as $key => $value){
			array_push($combos, new Combo($value["id"], $value["usuario"]));
		}
		if(!empty($combos))
			return new Respuesta(1, $combos);
		else
			return new Respuesta(-1, "no se encontraron datos");
	}
	public static function leerUsuario(int $id){
		$rta = DB::leerUsuario($id)[0];
		return new Usuario($id, $rta["razon_social"], $rta["nombre"], $rta["apellido"], $rta["cuit"], $rta["fk_condicion_iva"], $rta["fk_perfil"], $rta["ruta"],$rta["fechaAlta"], $rta["fechaActualizacion"]);
	}
	public static function altaUsuario($mail, $clave, $ruta, $nombreUsuario){
		$control = DB::controlarMail($mail);
		if($control[0]['existe'] == 0){
			$rta = DB::altaUsuario($mail, $clave, $ruta, $nombreUsuario);
			if($rta > 0){
				return new Respuesta($rta, "alta correcta");
			}
			else
				return new Respuesta(-1, "no se pudo realizar el alta");
		}
		else
			return new Respuesta(-2, 'el mail ya existe');
	}
	public static function actualizarUsuario($id,	$mail, $clave, $ruta, $nombreUsuario ){
		$usuario = DB::leerUsuarioMail($mail)[0];
		if(empty($usuario) || $usuario['id'] == $id){
			$rta = DB::actualizarUsuario($mail, $clave, $ruta, $nombreUsuario, $id);

			return new Respuesta(1, "se realizó la actualización");
		}
		return new Respuesta(-1, 'no se pudo realizar la actualización');

	}
	public static function login($mail, $clave){
		$usuario = DB::leerUsuarioMail($mail);
		if(!empty($usuario)){
			if(password_verify($clave, $usuario[0]['clave']))
			{
				//var_dump($usuario[0]['ruta']);die();
				$user = Self::leerUsuarioApi($usuario[0]['ruta'],$usuario[0]['id']);
				if($user->getCodigo() > 0){
					$user = $user->getMensaje();
					//var_dump($user);die();
					$user['ruta']=$usuario[0]['ruta'];
					return new Respuesta(1,$user);
				}
				else
					return $user;
			}
			else
				return new Respuesta(-2, "Los datos ingresados no corresponden");
		}
		else
			return new Respuesta(-1, "No se encontro el mail ingresado");
	}
	public static function leerUsuarioMail($mail){
		$rta = DB::leerUsuarioMail($mail);
		if(!empty($rta)){
			$rta = $rta[0];

			return new Usuario($rta['id'], $rta["razon_social"], $rta["nombre"], $rta["apellido"], $rta["cuit"], $rta["fk_condicion_iva"], $rta["fk_perfil"], $rta["ruta"],$rta["fechaAlta"], $rta["fechaActualizacion"]);
		}
	}

	public function controlarMail($mail){
		$rta = Self::leerUsuarioMail($mail);
		return ($rta) ? new Respuesta(-1, 'el mail se encuentra registrado'): new Respuesta(1, 'el mail no se encuentra registrado');
	}
	private static function leerUsuarioApi($ruta, $idUsuario){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $ruta."/apis/usuarios/v1/?id=".$idUsuario,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  return new Respuesta(-1, "cURL Error #:" . $err);
		} else {
		//	 var_dump($response);die();
		  return new Respuesta(1, json_decode($response, true));
		}	
	}
	public static function cambiarClave($idUsuario, $claveActual, $claveNueva){
		$usr = DB::leerUsuario($idUsuario);
		if(!empty($usr)){
			if(password_verify($claveActual, $usr[0]['clave'])){
				DB::actualizarClave($idUsuario, $claveNueva);
				return new Respuesta(1, 'se realizó el cambio de clave');
			}
			else
				return new Respuesta(-2, 'clave incorrecta');
		}
		else return new Respuesta(-1, 'no se encontró el usuario');
	}
	public static function olvideMiclave($mail){
		$usuario = Self::leerUsuarioMail($mail);

		if(!empty($usuario)){
			$ruta = str_replace('www.','',$usuario->getRuta());
			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://www.".$ruta."/apis/usuarios/v1/?mail=".$mail,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "cache-control: no-cache"
		  ),
			));
			
			$response = curl_exec($curl);
			$response = json_decode($response, true)['mensaje'];
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  return new Respuesta(-1,"cURL Error #:" . $err);
			} else {
			  return new Respuesta(1,$response);
			}
		}
		else
			return new Respuesta(-1, 'No se encontró el mail relacionado a un usuario de Factura Print');
	}
	public static function blanqueoClave($tkn, $clave){
		$verificacion = JWT::validar($tkn);
		if($verificacion->getCodigo()>0){
			$verificacion = $verificacion->getMensaje();
			$usuario = DB::leerUsuarioMail($verificacion->mail);
			$rta = DB::actualizarClave($usuario[0]['id'], $clave);
			if(!empty($rta))
				return new Respuesta(1, 'se actualizó la clave');
			else
				return new Respuesta(-1, 'No pudimos actualizar tu clave');
		}
		else
			return $verificacion;
	}
}
?>