<?php
namespace admin\code\usuarios;
use admin\MyUFrame\Base;
class DB extends Base{
	public static function usuario(){
		$query = "SELECT id,razon_social, nombre, apellido, cuit, fk_condicion_iva, fk_perfil, ruta FROM usuarios_usuarios";
		return Self::ejecutarConsulta($query);
	}
	public static function leerUsuario(int $id){
		$query = "SELECT id, mail, ruta, clave FROM usuarios_usuarios WHERE id = ?";
		return Self::ejecutarConsulta($query, array($id));
	}
	public static function leerUsuarioMail(string $mail){
		$query = "SELECT id, fechaAlta, fechaActualizacion, mail, clave, ruta FROM usuarios_usuarios WHERE mail = ?";
		return Self::ejecutarConsulta($query, array($mail));
	}
	public static function altaUsuario($mail, $clave,$ruta, $nombreUsuario){
		$query = "INSERT INTO usuarios_usuarios(mail, clave, ruta, fechaAlta, fechaActualizacion) VALUES (?,?,?,?,?)";
		return Self::ejecutarConsulta($query, array($mail,$clave, $ruta,date("Y-m-d H:i:s"),date("Y-m-d H:i:s")));
	}
	public static function actualizarUsuario($mail, $clave, $ruta, $nombreUsuario,  $id){
		$query = "UPDATE usuarios_usuarios SET mail = ?,  ruta= ?, nombreUsuario= ?, fechaActualizacion = ? WHERE id = ?";
		Self::ejecutarConsulta($query, array($mail,  $ruta, $nombreUsuario,date("Y-m-d H:i:s"), $id));
	}
	public static function controlarMail($mail){
		$query = "SELECT COUNT(id) as existe FROM usuarios_usuarios WHERE mail = ?";
		return Self::ejecutarConsulta($query,array($mail));
	}
	public static function actualizarClave($idUsuario, $clave){
		$query = "UPDATE usuarios_usuarios SET clave = ? WHERE id = ?";
		return Self::ejecutarConsulta($query, array(password_hash($clave, PASSWORD_BCRYPT), $idUsuario));
	}
	public static function storeFcm($idUsuario, $fcmId, $sistemaOperativo, $vesionSistemaOperativo){
		$query = "INSERT INTO `usuarios_fcm`(`fechaAlta`, fechaActualizacion,`id_usuario`, `fcm_id`, `sistema_operativo`, `version_sistema_operativo`) VALUES (?,?,?,?,?,?)";
		
		return Self::ejecutarConsulta($query, [date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),$idUsuario, $fcmId, $sistemaOperativo, $vesionSistemaOperativo]);
	}
	public static function showFcm($fcmId){
		$query = "SELECT `id`, `fechaAlta`, `fechaActualizacion`, `id_usuario`, `fcm_id`, `sistema_operativo`, `version_sistema_operativo` FROM `usuarios_fcm` WHERE fcm_id = ?";
		return Self::ejecutarConsulta($query,[$fcmId]);
	}
}
?>