<?php
namespace admin\code\usuarios;
use admin\MyUFrame\Auth\JWT;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Respuesta;
use code\repositorios\v2\CtrlRepositorio;
class CtrlUsuarioFcm extends Controller {
	
	public static function store($request){
		$fcm = DB::showFcm($request->fcmId);
		if(!empty($fcm))
			return new Respuesta($fcm[0]['id'],'el fcmId ya se encuentra registrado');
		
		$id = DB::storeFcm($request->idUsuario, $request->fcmId, $request->sistemaOperativo, $request->versionSistemaOperativo);

		if(!empty($id))
			return new Respuesta($id, 'se guardaron los datos del fcm');

		return new Respuesta(-1, 'no se guardaron los datos');
	}
}