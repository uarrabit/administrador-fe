<?php
namespace code\administradores;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
class CtrlAdministrador extends Controller {
	public function administrador(){
		$rta = DB::administrador();
		$combos = [];
		foreach($rta as $key => $value){
			array_push($combos, new Combo($value["id"], $value["administrador"]));
		}
		if(!empty($combos))
			return new Respuesta(1, $combos);
		else
			return new Respuesta(-1, "no se encontraron datos");
	}
	public static function leerAdministrador(int $id){
		$rta = DB::leerAdministrador($id)[0];
		return new Administrador($id, $rta["nombre"], $rta["apellido"], $rta["mail"], $rta["clave"],$rta["fechaAlta"], $rta["fechaActualizacion"]);
	}
	public static function altaAdministrador($nombre, $apellido, $mail, $clave){
		$control = DB::controlarMail($mail);
		if($control[0]['existe'] == 0){
			if(!empty($clave))
				$rta = DB::altaAdministrador($nombre, $apellido, $mail, $clave);
			else
				return new Respuesta(-3, 'la clave no puede estar vacía');
		if($rta > 0)
			return new Respuesta($rta, "alta correcta");
		else
			return new Respuesta(-1, "no se pudo realizar el alta");
		}
		else
			return new Respuesta(-2, 'el mail ya existe');
	}
	public static function actualizarAdministrador(int $id, $nombre, $apellido, $mail, $clave){
		$rta = DB::actualizarAdministrador($nombre, $apellido, $mail, $clave, $id);
		return new Respuesta(1, "se realizó la actualización");
	}

	public static function login($mail, $clave){
		$admin = DB::leerAdminMail($mail);
		if(!empty($admin)){
			if(password_verify($clave, $admin[0]['clave']))			
				return new Respuesta(1,$admin);
			else
				return new Respuesta(-2, "Los datos ingresados no corresponden");
		}
		else
			return new Respuesta(-1, "No se encontro el mail ingresado");
	}
	public static function actualizarClave($claveVieja, $claveNueva, $idAdmin){
		try {
			$user = DB::leerAdministrador($idAdmin)[0];
			if(password_verify($claveVieja, $user['clave'])){
				if(!empty($claveNueva))
					$rta = DB::actualizarClave($idAdmin, $claveNueva);
				else
					return new Respuesta(-2, 'la clave no puede estar vacía');
				if($rta > 0)
					return new Respuesta(1, 'se actualizó la clave');
				else
				 return new Respuesta(-1, 'no se pudo realizar la actualización');
			}
			else
				return new Respuesta(-2, "clave incorrecta");

		} catch (Exception $e) {
			return new Respuesta(-1* $e->code, $e->message);
		}

	}
}
?>