<?php
	namespace code\administradores;
	use admin\MyUFrame\Clase;
	class Administrador extends Clase
	{
		private $id;
		private $nombre;
		private $apellido;
		private $mail;
		private $clave;
		
		public function __construct($id, $nombre, $apellido, $mail, $clave){
			$this->id = sprintf("%s",$id);
        	$this->nombre = sprintf("%s",$nombre);
        	$this->apellido = sprintf("%s",$apellido);
        	$this->mail = sprintf("%s",$mail);
        	$this->clave = sprintf("%s",$clave);
        }
		public function setId($value){
			$this->id = sprintf("%s",$value);
		}
		public function setNombre($value){
			$this->nombre = sprintf("%s",$value);
		}
		public function setApellido($value){
			$this->apellido = sprintf("%s",$value);
		}
		public function setMail($value){
			$this->mail = sprintf("%s",$value);
		}
		public function setClave($value){
			$this->clave = sprintf("%s",$value);
		}
		public function getId(){
			return $this->id;
		}
		public function getNombre(){
			return $this->nombre;
		}
		public function getApellido(){
			return $this->apellido;
		}
		public function getMail(){
			return $this->mail;
		}
		public function getClave(){
			return $this->clave;
		}
		public function getJson(){
				$vars = get_object_vars($this);
				return Self::getArray($vars);
		}
	}
?>