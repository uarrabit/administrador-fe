<?php
namespace code\administradores;
use admin\MyUFrame\Base;
class DB extends Base{
	public static function administrador(){
		$query = "SELECT id,nombre, apellido, mail, clave FROM administradores_administradores";
		return Self::ejecutarConsulta($query);
	}
	public static function leerAdministrador(int $id){
		$query = "SELECT id,nombre, apellido, mail, clave, fechaAlta, fechaActualizacion FROM administradores_administradores WHERE id = ?";
		return Self::ejecutarConsulta($query, array($id));
	}
	public static function altaAdministrador($nombre, $apellido, $mail, $clave){
		$query = "INSERT INTO administradores_administradores(nombre, apellido, mail, clave, fechaAlta) VALUES (?,?,?,?,?)";
		return Self::ejecutarConsulta($query, array($nombre, $apellido, $mail, password_hash($clave, PASSWORD_BCRYPT),date("Y-m-d H:i:s")));
	}
	public static function actualizarAdministrador($nombre, $apellido, $mail, $clave, int $id){
		$query = "UPDATE administradores_administradores SET nombre = ?, apellido= ?, mail= ?, clave= ?, fechaActualizacion = ? WHERE id = ?";
		Self::ejecutarConsulta($query, array($nombre, $apellido, $mail, $clave,date("Y-m-d H:i:s"), $id));
	}
	public static function actualizarClave($idAdmin, $clave){
		$query = "UPDATE administradores_administradores SET clave = ? WHERE id = ?";
		return Self::ejecutarConsulta($query, array(password_hash($clave, PASSWORD_BCRYPT), $idAdmin));
	}
	public static function controlarMail($mail){
		$query = "SELECT COUNT(id) as existe FROM administradores_administradores WHERE mail = ?";
		return Self::ejecutarConsulta($query,array($mail));
	}
	public static function leerAdminMail($mail){
		$query = "SELECT id,nombre, apellido, mail, clave, fechaAlta, fechaActualizacion FROM administradores_administradores WHERE mail = ?";
		return Self::ejecutarConsulta($query, array($mail));
	}
}
?>