<?php
namespace admin\code\migraciones;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
use admin\MyUFrame\Controller;
use code\repositorios\CtrlRepositorio;
class CtrlMigracion extends Controller {
	public static function ejecutarPendientes(){
		$migraciones = json_decode(file_get_contents(dirname(__FILE__).str_replace('/', DIRECTORY_SEPARATOR , '/migraciones.json')));
		//include dirname(__FILE__).'/../../migraciones/sist/20181010194220.php';
		$ultima = DB::leerUltimoRegistro()[0]['migracion'];
		$flag = 0;
		foreach ($migraciones->migraciones as $key => $value) {
			if(!empty($ultima)){
				if($flag == 0){
					if($value == $ultima)
						$flag = 1;
				}
				else {
					$value= str_replace('\\', DIRECTORY_SEPARATOR , $value);
					$value= str_replace('/', DIRECTORY_SEPARATOR , $value);
				include dirname(__FILE__).'/../../migraciones/'.$value.'.php';
				DB::registrarMigracion($value);
				}
			}
			else {
				include dirname(__FILE__).'/../../migraciones/'.$value.'.php';
				DB::registrarMigracion($value);
			}
		}
	}
	public static function correrMigracion($tabla, $columnas){
		$tabla = strtolower($tabla);
		DB::crearTabla($tabla);
		foreach ($columnas as $key => $value) {
			DB::crearColumnas($value['nombre'], $value['tipo_dato'], $tabla);
		}		
	}
	public static function migracionTablaCompuesta($tabla, $columnas, $pk){
		DB::crearTablaVacia($tabla);
		$pkColumns = "";
		foreach ($pk as $key => $value) {
			DB::crearColumnas($value['nombre'], $value['tipo_dato'], $tabla);
			if(empty($pkColumns))
				$pkColumns .= $value['nombre'];
			else
				$pkColumns .= ', '. $value['nombre'];
		}
		DB::pk($tabla, $pkColumns);
		foreach ($columnas as $key => $value) {
			DB::crearColumnas($value['nombre'], $value['tipo_dato'], $tabla);
		}
	}
	public static function ejecutarSQL($sql){
		$array = explode(';', $sql);
		foreach ($array as $key => $value) {
			DB::SQL($value);
		}
		
	}

	public static function correrMigracionTotal(){
		$repos = CtrlRepositorio::repositoriosEnProduccion();
		foreach ($repos as $key => $value) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => str_replace('fceweb.com.ar','secreya.com',$value)."/code/migraciones/ejecutarMigraciones.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);

		curl_close($curl);
		}
	}
}
?>