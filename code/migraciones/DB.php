<?php
namespace admin\code\migraciones;
use admin\MyUFrame\Base;
class DB extends Base{
	public static function leerUltimoRegistro(){
		$query = "SELECT `migracion`FROM `migraciones` ORDER BY id DESC LIMIT 1";
		return Self::ejecutarConsulta($query);
 	 }
	/**
	*@var columnas tine que llegar con el tipo de dato [0]=>[nombre]=>['tipo_dato']
	*/
	public static function crearTabla(string $nombre){
		$query = "CREATE TABLE IF NOT EXISTS ".Self::DB .'.'. $nombre." 
		(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`fechaAlta` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
		`fechaActualizacion` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00')
		ENGINE = InnoDB;
		";
		Self::ejecutarConsulta($query);
	}
	public static function pk($tabla, $columnas){
		$query = "ALTER TABLE $tabla ADD PRIMARY KEY($columnas)";
		Self::ejecutarConsulta($query);
	}
	public static function crearColumnas(string $columna,string $tipo_dato, string $tabla){
		$query = "ALTER TABLE $tabla ADD $columna $tipo_dato";
		//var_dump($query);
		Self::ejecutarConsulta($query);
	}
	public static function crearTablaVacia(string $nombre){
		$query = "CREATE TABLE IF NOT EXISTS ".Self::DB .'.'. $nombre." (`fechaAlta` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
		`fechaActualizacion` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00')
				 ENGINE = InnoDB;
		";
		Self::ejecutarConsulta($query);
	}
	public static function registrarMigracion($nombre)
	{
		$query = "INSERT INTO `migraciones`(`migracion`) VALUES (?)";
		return Self::ejecutarConsulta($query, array($nombre));
	}
	public static function SQL($sql){
		return Self::ejecutarConsulta($sql);
	}
}
?>