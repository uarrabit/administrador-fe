<?php
namespace code\repositorios;

//include dirname(__FILE__).'/vendor/autoload.php';
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
//use Cz\Git\GitRepository;

class CtrlRepositorio extends Controller {

	public static function repositorios(){
		$rta = DB::repositorios();
		$repos = [];
		foreach ($rta as $key => $value) {
			array_push($repos, $value['ruta']);
		}
		return $repos;
	}
	public static function todosLosRepos(){
		$rta = [];
		$i = 1;
		foreach (Self::repositorios() as $key => $value) {
			array_push($rta, new Combo($i, $value));
			$i++; 
		}
		return new Respuesta(1, $rta);
	}
	public static function repositoriosEnProduccion(){
		$repos = Self::repositorios();
		$reposEnProduc = [];
		foreach ($repos as $key => $value) {
			$conf = Self::leerConfiguracionCliente($value);
			if($conf['test'] != 'TRUE')
				array_push($reposEnProduc, $value);
		}
		return $reposEnProduc;
	}
	public static function leerConfiguracionCliente($ruta){
		$curl = curl_init();
				curl_setopt_array($curl, array(
		CURLOPT_URL => $ruta."/apis/configuraciones/v1/",
			CURLOPT_RETURNTRANSFER => true,
		 	CURLOPT_ENCODING => "",
		 	CURLOPT_MAXREDIRS => 10,
		 	CURLOPT_TIMEOUT => 30,
		 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		 	CURLOPT_CUSTOMREQUEST => "GET",
		 	CURLOPT_POSTFIELDS => "",
		 	CURLOPT_HTTPHEADER => array(
		   "Content-Type: application/json",
		   "cache-control: no-cache"
		 ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return json_decode($response, true);
		}
	}

	public static function alctualizarBaseDatos(){
		$repos = Self::repositorios();
		$envios= '';
		foreach ($repos as $key => $value) {

			//$value = str_replace('fceweb.com.ar', 'secreya.com', $value);
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "$value/code/migraciones/ejecutarMigraciones.php",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$envios .= $value.'\n';
		}
		return new Respuesta(1,$envios);
	}
/*	public static function pruebaActualizarRepos(){
		var_dump('AJAJAJJA');die();
		$git = new GitRepository('/home/fcewebcom/public_html/20258081480');
		$git->pull('Origin');
	}*/
}
?>