<?php
namespace code\repositorios\v2;
use admin\MyUFrame\Base;
class DB extends Base{
	public static function repositorio($idEstado){
		$query = "SELECT id,ruta, idEstado FROM repositorios WHERE idEstado = ?";
		return Self::ejecutarConsulta($query,[$idEstado]);
	}
	public static function leerRepositorio(int $id){
		$query = "SELECT id,ruta, idEstado, fechaAlta, fechaActualizacion FROM repositorios WHERE id = ?";
		return Self::ejecutarConsulta($query, array($id));
	}
	public static function altaRepositorio($ruta, $idEstado){
		$query = "INSERT INTO repositorios(ruta, idEstado, fechaAlta) VALUES (?,?,?)";
		return Self::ejecutarConsulta($query, array($ruta, $idEstado,date("Y-m-d H:i:s")));
	}
	public static function actualizarRepositorio($idEstado, int $id){
		$query = "UPDATE repositorios SET idEstado= ?, fechaActualizacion = ? WHERE id = ?";
		Self::ejecutarConsulta($query, array( $idEstado,date("Y-m-d H:i:s"), $id));
	}
}
?>