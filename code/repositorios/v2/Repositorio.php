<?php
	namespace code\repositorios\v2;
	use admin\MyUFrame\Clase;
	class Repositorio extends Clase
	{
		private $id;
		private $ruta;
		private $idEstado;
		
		public function __construct($id, $ruta, $idEstado){
			$this->id = sprintf("%s",$id);
        	$this->ruta = sprintf("%s",$ruta);
        	$this->idEstado = sprintf("%s",$idEstado);
        }
		public function setId($value){
			$this->id = sprintf("%s",$value);
		}
		public function setRuta($value){
			$this->ruta = sprintf("%s",$value);
		}
		public function setIdEstado($value){
			$this->idEstado = sprintf("%s",$value);
		}
		public function getId(){
			return $this->id;
		}
		public function getRuta(){
			return $this->ruta;
		}
		public function getIdEstado(){
			return $this->idEstado;
		}
		public function getJson(){
				$vars = get_object_vars($this);
				return Self::getArray($vars);
		}
	}
?>