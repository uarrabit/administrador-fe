<?php
namespace code\repositorios\v2;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
class CtrlRepositorio extends Controller {

	CONST DISPONIBLE = 1;
	CONST UTILIZADO = 2;

	public function repositorio($idEstado){
		$idEstado = ($idEstado)?$idEstado:1;
		$rta = DB::repositorio($idEstado);
		$combos = [];
		foreach($rta as $key => $value){
			array_push($combos, new Combo($value["id"], $value["repositorio"]));
		}
		if(!empty($combos))
			return new Respuesta(1, $combos);
		else
			return new Respuesta(-1, "no se encontraron datos");
	}
	public static function leerRepositorio($id){
		$rta = DB::leerRepositorio($id)[0];
		return new Repositorio($id, $rta["ruta"], $rta["idEstado"],$rta["fechaAlta"], $rta["fechaActualizacion"]);
	}
	public static function altaRepositorio($ruta){
		$rta = DB::altaRepositorio('https://www.fceweb.com.ar/'.$ruta,Self::DISPONIBLE);
		if($rta > 0)
			return new Respuesta($rta, "alta correcta");
		else
			return new Respuesta(-1, "no se pudo realizar el alta");
	}
	public static function actualizarRepositorio($id,  $idEstado){
		$rta = DB::actualizarRepositorio($idEstado, $id);
		return new Respuesta(1, "se realizó la actualización");
	}
	public static function proximoRepo(){
		$repo = Self::repositorio(1);
		if($repo->getCodigo() == -1)
			return new Respuesta(-1, 'no se encontraron repositorios diponibles');
		return new Respuesta(1, Self::leerRepositorio($repo->getMensaje()[0]->getId()));
	}
	
}
?>