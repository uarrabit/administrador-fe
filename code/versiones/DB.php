<?php
namespace code\versiones;
use admin\MyUFrame\Base;
class DB extends Base{
	public static function version(){
		$query = "SELECT id,version, compativilidad_minima FROM versiones";
		return Self::ejecutarConsulta($query);
	}
	public static function leerVersion(){
		$query = "SELECT id,version, compativilidad_minima, fechaAlta, fechaActualizacion FROM versiones ORDER BY id DESC LIMIT 1";
		return Self::ejecutarConsulta($query);
	}

	public static function altaVersion($version, $compativilidad_minima){
		$query = "INSERT INTO versiones(version, compativilidad_minima, fechaAlta) VALUES (?,?,?)";
		return Self::ejecutarConsulta($query, array($version, $compativilidad_minima,date("Y-m-d H:i:s")));
	}
	public static function actualizarVersion($version, $compativilidad_minima, int $id){
		$query = "UPDATE versiones SET version = ?, compativilidad_minima= ?, fechaActualizacion = ? WHERE id = ?";
		Self::ejecutarConsulta($query, array($version, $compativilidad_minima,date("Y-m-d H:i:s"), $id));
	}
}
?>