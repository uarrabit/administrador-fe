<?php
	namespace code\versiones;
	use admin\MyUFrame\Clase;
	class Version extends Clase
	{
		private $id;
		private $version;
		private $compativilidad_minima;
		
		public function __construct($id, $version, $compativilidad_minima){
			$this->id = sprintf("%s",$id);
        	$this->version = sprintf("%s",$version);
        	$this->compativilidad_minima = sprintf("%s",$compativilidad_minima);
        }
		public function setId($value){
			$this->id = sprintf("%s",$value);
		}
		public function setVersion($value){
			$this->version = sprintf("%s",$value);
		}
		public function setCompativilidad_minima($value){
			$this->compativilidad_minima = sprintf("%s",$value);
		}
		public function getId(){
			return $this->id;
		}
		public function getVersion(){
			return $this->version;
		}
		public function getCompativilidad_minima(){
			return $this->compativilidad_minima;
		}
		public function getJson(){
			$vars = get_object_vars($this);
			return Self::getArray($vars);
		}
	}
?>