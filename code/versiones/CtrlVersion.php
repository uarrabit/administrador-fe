<?php
namespace code\versiones;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
class CtrlVersion extends Controller {
	public function version(){
		$rta = DB::version();
		$combos = [];
		foreach($rta as $key => $value){
			array_push($combos, new Combo($value["id"], $value["version"]));
		}
		if(!empty($combos))
			return new Respuesta(1, $combos);
		else
			return new Respuesta(-1, "no se encontraron datos");
	}
	public static function leerVersion(){
		$rta = DB::leerVersion()[0];
		return new Version($rta['id'], $rta["version"], $rta["compativilidad_minima"],$rta["fechaAlta"], $rta["fechaActualizacion"]);
	}
	public static function altaVersion($version, $compativilidad_minima){
		$rta = DB::altaVersion($version, $compativilidad_minima);
		if($rta > 0)
			return new Respuesta($rta, "alta correcta");
		else
			return new Respuesta(-1, "no se pudo realizar el alta");
	}
	public static function actualizarVersion(int $id, $version, $compativilidad_minima){
		$rta = DB::actualizarVersion($version, $compativilidad_minima, $id);
		return new Respuesta(1, "se realizó la actualización");
	}
	public static function comprobarVersion($version){
		$currentVesion= Self::leerVersion();
		return ($currentVesion->getCompativilidad_minima() <= $version) ? new Respuesta(1, "true") : new Respuesta(-1, "false");
		
	}
}
?>