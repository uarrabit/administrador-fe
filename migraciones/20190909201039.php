<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"nombre", "tipo_dato"=>"VARCHAR(60) NOT NULL"), array( "nombre" =>"apellido", "tipo_dato"=> "VARCHAR(60) NOT NULL"), array( "nombre" =>"mail", "tipo_dato"=> "VARCHAR(60) NOT NULL"), array( "nombre" =>"clave", "tipo_dato"=> "VARCHAR(60) NOT NULL"));
	CtrlMigracion::correrMigracion("administradores_administradores", $columnas);
?>