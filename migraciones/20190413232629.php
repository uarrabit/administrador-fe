<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"mail", "tipo_dato"=> "VARCHAR(80) NOT NULL"), array( "nombre" =>"clave", "tipo_dato"=> "VARCHAR(60) NOT NULL"), array( "nombre" =>"ruta", "tipo_dato"=> "VARCHAR(120) NOT NULL"), array("nombre"=>"nombre_usuario", "tipo_dato" => "VARCHAR(60) NOT NULL"));
	CtrlMigracion::correrMigracion("usuarios_usuarios", $columnas);
?>