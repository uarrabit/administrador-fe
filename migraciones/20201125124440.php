<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"version", "tipo_dato"=>"VARCHAR(45) NOT NULL"), array( "nombre" =>"compativilidad_minima", "tipo_dato"=> "VARCHAR(45) NOT NULL"));
	CtrlMigracion::correrMigracion("versiones", $columnas);
?>