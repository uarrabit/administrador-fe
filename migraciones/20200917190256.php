<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"ruta", "tipo_dato"=>"VARCHAR(512) NOT NULL"), array( "nombre" =>"idEstado", "tipo_dato"=> "TINYINT(2) NOT NULL DEFAULT(1)"));
	CtrlMigracion::correrMigracion("repositorios", $columnas);
?>