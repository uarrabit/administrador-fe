<?php 
	include_once dirname(__FILE__)."/../../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"migracion", "tipo_dato"=>"VARCHAR(45) NOT NULL"), array( "nombre" =>"fecha", "tipo_dato"=> "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"));
	CtrlMigracion::correrMigracion("migraciones", $columnas);
?>