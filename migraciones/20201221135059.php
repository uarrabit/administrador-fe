<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\migraciones\CtrlMigracion;
	$columnas = array( array( "nombre" =>"id_usuario", "tipo_dato"=>"INT NOT NULL"), array( "nombre" =>"fcm_id", "tipo_dato"=> "INT NOT NULL"), array( "nombre" =>"sistema_operativo", "tipo_dato"=> "VARCHAR(45) NOT NULL"), array( "nombre" =>"version_sistema_operativo", "tipo_dato"=> "VARCHAR(45) NOT NULL"));
	CtrlMigracion::correrMigracion("usuarios_fcm", $columnas);
?>