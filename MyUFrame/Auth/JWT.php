<?php 
namespace admin\MyUFrame\Auth;
include dirname(__FILE__).'/vendor/autoload.php';
use \Firebase\JWT\JWT as webT;
use admin\MyUFrame\Controller;
use \DateTime;
use \DateInterval;
use admin\MyUFrame\Respuesta;
use \Firebase\JWT\SignatureInvalidException;
use\Exception;
/**
 * 
 */
class JWT extends Controller
{
	CONST KEY = 'Hx4w8gC6JR';
	CONST TIEMPO_VIDA = 8 * 60; //minutos de vida
	
	#la data tiene que llegar como array
	public static function generarJWT($data = "", $TIEMPO_VIDA = Self::TIEMPO_VIDA){
		$venc = new DateTime("now");
		$intervalo = new DateInterval("PT". $TIEMPO_VIDA . "M");
		$venc->add($intervalo);		
		// $venc = date_create_from_format('Y-m-d H:i:s', '2018-07-16 14:20:00');
		$fecha = new DateTime("now");
		$token = array(
    	"iss" => $_SERVER['HTTP_HOST'],
    	"aud" => $_SERVER['HTTP_HOST'],
    	"data"=> $data,
    	"iat" => $fecha->format('U'),
    	"nbf" => $fecha->format('U'),
    	"exp" => $venc->format('U'));
    	//var_dump($token);die;
    	return  webT::encode($token, Self::KEY);
	}
	public static function decodificarJWT($JWT){
		return webT::decode($JWT, Self::KEY, array('HS256'));
	}
	public static function validarToken($JWT, $api){
		if(!empty($JWT)){
			try{
				$token = Self::decodificarJWT($JWT);
				$vencimiento = date_create_from_format('U', $token->nbf);
				$ahora = new DateTime();
				$permisos = $token->data->permisos;
				$permiso_api = DB::leerApi($api)[0];
				$flag = 0;
				foreach ($permisos as $key => $value) {
					if($value->id == $permiso_api['pk']){
						$flag = 1;
						break;
					}
				}
				if($flag == 1){
					if(isset($token->data->versionApp)){							
						if($token->data->versionApp >= $permiso_api['version_minima'])
							return new Respuesta(1, 'token válido');
						else
							return new Respuesta(-503, 'debes actualizar la aplicacion para acceder');
					}
					else
						 return new Respuesta(1, 'token válido');
				}
				else
					return new Respuesta(-502, 'no tienes permiso para acceder');
			}
			catch(Exception $e){
				return new Respuesta(-400, $e->getMessage());
			}
		}
		else
			return new Respuesta(-400, 'Token Inválido');
	}
	public static function validar($JWT){
		if(!empty($JWT)){
			try{
				$token = Self::decodificarJWT($JWT);
				$vencimiento = date_create_from_format('U', $token->nbf);
				$ahora = new DateTime();
				if($vencimiento->format('Y-m-d H:i:s') > $ahora->format('Y-m-d H:i:s'))
					return new Respuesta(1, $token->data);
				else
					return new Respuesta(-401, 'token vencido, repita el proceso');
			}
			catch(Exception $e){
				return new Respuesta(-400, $e->getMessage());
			}
		}
		else
			return new Respuesta(-400, 'Token Inválido');
	}

}
?>