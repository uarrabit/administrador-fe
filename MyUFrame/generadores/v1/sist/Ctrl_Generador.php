<?php 
namespace MyUFrame\generadores\v1\sist;
use admin\MyUFrame\Controller;
use admin\MyUFrame\Respuesta;
/**
 * 
 */
class Ctrl_Generador extends Controller
{
	public static function generarClase($arr_parametros, $namespace, $clase){
		$codigo = 
		"<?php
	namespace ". $namespace.";
	use admin\MyUFrame\Clase;
	class ". ucwords($clase) ." extends Clase
	{
		". Self::prepararParametrosClase($arr_parametros). Self::generarSeters($arr_parametros). Self::generarGeters($arr_parametros)."
	}
?>";
		Self::generarArchivo($codigo, $namespace, ucwords($clase));
		return new Respuesta(1,'se generó el archivo');		
	}
	private static function prepararParametrosClase($arr_parametros){
		$codigo= "";
		foreach ($arr_parametros as $key => $value) {
			$codigo .= 'private $'."$value;
		";
		}
		$x = '';
		foreach ($arr_parametros as $key => $value) {
			 if(!empty($x))
                $x.=', ';
			$x .='$'."$value";
		}
		$codigo .= "
		public function __construct($x){
		";
		foreach ($arr_parametros as $key => $value) {
                $codigo .= '	$this->' . $value . ' = sprintf("%s",$' . $value . ');
        ';
		}
		$codigo .="}
		";

		return $codigo;
	}
	private static function generarSeters($arr_parametros){
		$codigo = "";
		foreach ($arr_parametros as $key => $value) {
			$codigo .= "public function set".ucfirst($value).'($value)'.'{
			$this->'."$value".' = sprintf("%s",$value);
		}
		';
		}
		return $codigo;
	} 
	private static function generarGeters($arr_parametros){
		$codigo = "";
		foreach ($arr_parametros as $key => $value) {
			$codigo .= "public function get".ucfirst($value).'()'.'{
			return $this->'."$value;
		}
		";
		}
		$codigo .= 'public function getJson(){
				$vars = get_object_vars($this);
				return Self::getArray($vars);
		}';
		return $codigo;
	} 
	private static function generarArchivo($archivo, $namespace, $clase){
		$ruta = dirname(__FILE__). DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
		$namespace = str_replace("\\\\", DIRECTORY_SEPARATOR, $namespace);
		$parts = explode(DIRECTORY_SEPARATOR, $ruta.$namespace);
        $dir = '';
        foreach($parts as $part){        	
        		$dir .= $part. DIRECTORY_SEPARATOR;
        	if(!is_dir($dir)) 
        		mkdir($dir);
        }
        file_put_contents($dir.$clase.'.php', $archivo);
	}
	##########################################################################################

	/**
	*@generar APIs
	*@author: Arrabit, Ulises
	*@date 2018-10-09
	*@var ruta donde se va a alojar la API
	*@var controladora es la ruta de la controladora, lo que le seguiría al USE
	**/
	public static function generarApi(string $ruta,string $nombre, array $parametros,string $controladora){
		$codigo = 
		'<?php
	include_once dirname(__FILE__).str_replace("\\\\", DIRECTORY_SEPARATOR,"'. Self::obtenerRutaAutoload($ruta).'");
	use '.$controladora.';
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		if(empty($_GET["id"]))
			print( json_encode('. Self::retirarNamespace($controladora) .'::'.$nombre.'()->getJson() ) ) ;
		else
			print( json_encode('.Self::retirarNamespace($controladora).'::leer'.ucwords($nombre).'($_GET["id"])->getJson() ) );
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		 $body = json_decode(file_get_contents("php://input"), true);
		 print( json_encode('.Self::retirarNamespace($controladora) .'::alta'.ucwords($nombre).'('.Self::prepararParametros($parametros).')->getJson()));
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		$body = json_decode(file_get_contents("php://input"), true);
		print( json_encode('.Self::retirarNamespace($controladora) .'::actualizar'.ucwords($nombre).'($body["id"],'.Self::prepararParametros($parametros).')->getJson()));
	}
?>';
		Self::generarArchivo($codigo, $ruta, 'index');
	}
	private static function prepararParametros(array $parametros){
		$rta = "";
		foreach ($parametros as $key => $value) {
			if(empty($rta))
				$rta .= '$body["'.$value.'"]';
			else
				$rta .= ', $body["'.$value.'"]';
		}
		return $rta;
	}
	private static function retirarNamespace(string $ruta){
		$array = explode('\\', $ruta);
		return end($array);
	}
	private static function obtenerRutaAutoload($ruta){
		$ruta = str_replace('admin\\', '', $ruta);
		$detalle_ruta = explode(DIRECTORY_SEPARATOR, $ruta);
		$i=0;
		$ruta_autoload = DIRECTORY_SEPARATOR;
		while ($i < count($detalle_ruta)) {
			$i++;
			$ruta_autoload .= '..'. DIRECTORY_SEPARATOR;
		}
		return $ruta_autoload . 'autoload.inc.php';
	}
#############################################################################################
	/**
	*@generar Migraciones
	*@author: Arrabit, Ulises
	*@date 2018-10-09
	*@var ruta donde se va a alojar la API
	*@var parametros  tine que llegar con el tipo de dato [0]=>[nombre]=>['tipo_dato']
	*/

	public static function generarMigracion($tabla, $parametros){
		$tabla = strtolower($tabla);
		$codigo= '<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\code\\migraciones\\CtrlMigracion;
	'.Self::prepararParametrosMigracion($parametros).'
	CtrlMigracion::correrMigracion("'.$tabla.'", $columnas);
?>';
		$ruta = 'migraciones';
		$nombre = /*'sist\\'.*/date('YmdHis');
		$migraciones = (array)json_decode(file_get_contents(dirname(__FILE__).'/../../../../code/migraciones/migraciones.json'));
		array_push($migraciones['migraciones'], $nombre);
		//print(json_encode($migraciones));die();
		file_put_contents(dirname(__FILE__).'/../../../../code/migraciones/migraciones.json', json_encode($migraciones));
		Self::generarArchivo($codigo, $ruta, $nombre);
	}


	/**
	*@method para poder crear una migracion que no contiene ni fechas ni id, se utiliza para generar tablas con claves compuestas
	*@author: Arrabit, Ulises
	*@date: 2018-10-12
	*/
	public static function migracionTablaCompuesta($tabla, $parametros, $pk){
			$codigo= '<?php 
	include_once dirname(__FILE__)."/../autoload.inc.php";
	use admin\\code\\migraciones\\CtrlMigracion;
	'.Self::prepararParametrosMigracion($parametros).'
	'.Self::prepararParametrosMigracion($pk, 'pk').'
	CtrlMigracion::migracionTablaCompuesta("'.$tabla.'", $columnas, $pk);
?>';
		$ruta = 'migraciones';
		$nombre = /*'sist\\'.*/date('YmdHis');
		$migraciones = (array)json_decode(file_get_contents(dirname(__FILE__).'/../../../../code/migraciones/migraciones.json'));
		array_push($migraciones['migraciones'], $nombre);
		//print(json_encode($migraciones));die();
		file_put_contents(dirname(__FILE__).'/../../../../code/migraciones/migraciones.json',json_encode($migraciones));
		Self::generarArchivo($codigo, $ruta, $nombre);
	}


	private static function prepararParametrosMigracion($parametros,$nombre = 'columnas'){
		$rta = "";
		foreach($parametros as $key => $value){
			if(empty($rta))
				$rta .= '$'.$nombre.' = array( array( "nombre" =>"'.$value['nombre'].'", "tipo_dato"=>"'.$value['tipo_dato'].'")';
			else
				$rta .= ', array( "nombre" =>"'. $value["nombre"].'", "tipo_dato"=> "'.$value['tipo_dato'].'")';
		}
		$rta .= ');';
		return $rta;
	}

#################################################################################################

	/**
	*@generar Controladora
	*@author: Arrabit, Ulises
	*@date 2018-10-09
	*
	*/
	public static function generarControladora(string $namespace, string $nombre, array $parametros, string $tabla){
		$codigo = '<?php
namespace '. $namespace.';
use admin\MyUFrame\Controller;
use admin\MyUFrame\Combo;
use admin\MyUFrame\Respuesta;
class Ctrl'.ucwords($nombre).' extends Controller {
	public function '.$nombre.'(){
		$rta = DB::'.$nombre.'();
		$combos = [];
		foreach($rta as $key => $value){
			array_push($combos, new Combo($value["id"], $value["'.$nombre.'"]));
		}
		if(!empty($combos))
			return new Respuesta(1, $combos);
		else
			return new Respuesta(-1, "no se encontraron datos");
	}
	public static function leer'.ucwords($nombre). '(int $id){
		$rta = DB::leer'.ucwords($nombre).'($id)[0];
		return new '. ucwords($nombre) . '('.Self::prepararParametrosObjeto($parametros).');
	}
	public static function alta'.ucwords($nombre).'('.Self::parametros($parametros).'){
		$rta = DB::alta'.ucwords($nombre).'('.Self::parametros($parametros).');
		if($rta > 0)
			return new Respuesta($rta, "alta correcta");
		else
			return new Respuesta(-1, "no se pudo realizar el alta");
	}
	public static function actualizar'.ucwords($nombre).'(int $id, '.Self::parametros($parametros).'){
		$rta = DB::actualizar'.ucwords($nombre).'('.Self::parametros($parametros).', $id);
		return new Respuesta(1, "se realizó la actualización");
	}
}
?>';
	Self::generarDB($namespace,  $nombre,  $parametros,  $tabla);
	Self::generarArchivo($codigo, $namespace, 'Ctrl'.ucwords($nombre));
	}
	private static function prepararParametrosObjeto(array $parametros){
		$string = '$id';
		foreach ($parametros as $key => $value) {
			if(empty($string))
				$string .= '$rta["'.$value.'"]';
			else
				$string .= ', $rta["'. $value.'"]';
		}
		$string .= ',$rta["fechaAlta"], $rta["fechaActualizacion"]';
		return $string;
	}
	
	private static function parametros(array $parametros){
		$string = "";
		foreach ($parametros as $key => $value) {
			if(empty($string))
				$string .= '$'.$value;
			else
				$string .= ', $'. $value;
		}
		return $string;
	}
################################################################################################
	/**
	*@generar Controladora Base de Datos
	*@author: Arrabit, Ulises
	*@date 2018-10-10
	*/
	private static function generarDB(string $namespace,string $nombre, array $parametros, string $tabla){
		$codigo ='<?php
namespace '.$namespace.';
use admin\\MyUFrame\\Base;
class DB extends Base{
	public static function '.$nombre.'(){
		$query = "SELECT id,'.Self::parametrosQuery($parametros).' FROM '. $tabla.'";
		return Self::ejecutarConsulta($query);
	}
	public static function leer'.ucwords($nombre).'(int $id){
		$query = "SELECT id,'.Self::parametrosQuery($parametros).', fechaAlta, fechaActualizacion FROM '. $tabla.' WHERE id = ?";
		return Self::ejecutarConsulta($query, array($id));
	}
	public static function alta'.ucwords($nombre).'('.Self::parametros($parametros).'){
		$query = "INSERT INTO '.$tabla. '('.Self::parametrosQuery($parametros).', fechaAlta) VALUES ('.Self::contarParametros($parametros) .',?)";
		return Self::ejecutarConsulta($query, array('.Self::parametros($parametros).',date("Y-m-d H:i:s")));
	}
	public static function actualizar'.ucwords($nombre).'('.Self::parametros($parametros).', int $id){
		$query = "UPDATE '.$tabla.' SET '.Self::parametrosUpdate($parametros). ', fechaActualizacion = ? WHERE id = ?";
		Self::ejecutarConsulta($query, array('.Self::parametros($parametros).',date("Y-m-d H:i:s"), $id));
	}
}
?>';
	Self::generarArchivo($codigo, $namespace, 'DB');
	}
	private static function parametrosUpdate(array $parametros){
		$string = "";
		foreach ($parametros as $key => $value) {
			if(empty($string))
				$string .= $value.' = ?';
			else
				$string .= ', '. $value .'= ?';
		}
		return $string;
	}
	private static function contarParametros($parametros){
		$string = "";
		for($i = 0; $i < COUNT($parametros); $i++){
			if(empty($string))
				$string .= '?';
			else
				$string .= ',?';
		}
		return $string;
	}
	private static function parametrosQuery(array $parametros){
		$string = "";
		foreach ($parametros as $key => $value) {
			if(empty($string))
				$string .= $value;
			else
				$string .= ', '. $value;
		}
		return $string;
	}

##########################################################################################
	/**
	*@generar Modelo (controladoras, clases, Apis, Migraciones)
	*@author: Arrabit, Ulises
	*@var parametros  tine que llegar con el tipo de dato [0]=>[nombre]=>['tipo_dato']
	*@date 2018-10-10
	*/
	public static function makeModel(string $namespace, string $nombre, array $parametros, string $tabla){
		Self::generarMigracion($tabla, $parametros);
		#le saco el tipo de dato a los parámetros
		$parametros = Self::parametrosNombre($parametros);
		$parametrosClase = $parametros;
		array_unshift($parametrosClase, 'id');
		Self::generarClase($parametrosClase, 'code\\'.$namespace, $nombre);
		Self::generarApi('apis\\'.$namespace,$nombre, $parametros, 'code\\'.$namespace.'\\Ctrl'.ucwords($nombre));
		Self::generarControladora('code\\'.$namespace, $nombre,  $parametros,  $tabla);
		return new Respuesta(1, 'se generó el modelo');
	}
	private static function parametrosNombre($parametros){
		$rta = [];
		foreach ($parametros as $key => $value) {
			array_push($rta, $value['nombre']);
		}
		return $rta;
	}
	
}
?>