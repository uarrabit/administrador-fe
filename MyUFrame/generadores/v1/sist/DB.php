<?php
namespace MyUFrame\generadores\sist;
use MyUFrame\Base;
class DB extends Base
{
	/**
	*@var columnas tine que llegar con el tipo de dato [0]=>[nombre]=>['tipo_dato']
	*/
	public static function crearTabla(string $nombre){
		$query = "CREATE TABLE IF NOT EXISTS ?(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`fechaAlta` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
		`fechaActualizacion` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00')
		ENGINE = InnoDB;
		";
		Self::ejecutarConsulta($query, array(Self::DB .'.'. $nombre));
	}
	public static function agregarColumna(string $columna,string $tipo_dato, string $tabla){
		$query = "ALTER TABLE ? ADD ? ?";
		Self::ejecutarConsulta($query, array($tabla, $columna, $tipo_dato));
	}
}
?>