<?php 
include dirname(__FILE__).'/../../../autoload.inc.php';
use MyUFrame\generadores\v1\sist\Ctrl_Generador;
header('Access-Control-Allow-Origin: *');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//$auth = apache_request_headers()['Token'];
	 $body = json_decode(file_get_contents("php://input"), true);
	print(json_encode(Ctrl_Generador::migracionTablaCompuesta($body['tabla'], $body['parametros'], $body['primaryKey'])));
}
?>