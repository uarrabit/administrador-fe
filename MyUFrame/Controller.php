<?php 
namespace admin\MyUFrame;
use \DateTime;
/**
 * 
 */
class Controller 
{
	const RUTA = '/../public/imagenes/';
	public static function array_sort($array, $on, $order=SORT_ASC)
	{
	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	            break;
	            case SORT_DESC:
	                arsort($sortable_array);
	            break;
	        }
	        $new_array= [];
	        foreach ($sortable_array as $k => $v) {
	           array_push($new_array, $array[$k]);
	        }
	    }

	    return $new_array;
	}
	public static function getJson($arr_obj){
		if(!empty($arr_obj)){
			$rta = [];
			foreach ($arr_obj as $key => $value) {
				if(!empty($value))
					array_push($rta, $value->getJson());
			}
			return $rta;
		}
		else
			return $arr_obj;
	}
	protected static function formatearFecha($fecha){
		if($fecha != '0000-00-00' && $fecha !='00-00-0000'){
			$fecha =new DateTime($fecha);
			return $fecha->format('Y-m-d');
		}
		return null;
	}
	protected static function eliminarTildes($cadena){
 
    //Codificamos la cadena en formato utf8 en caso de que nos de errores
    //$cadena = utf8_encode($cadena);
 
    //Ahora reemplazamos las letras
    $cadena = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
    );
 
    $cadena = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena );
 
    $cadena = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena );
 
    $cadena = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena );
 
    $cadena = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena );
 
    $cadena = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $cadena
    );
 
    return $cadena;
}
protected static function controlarCadena($string){
		$string = str_replace(" ", "_", $string);
		return Self::eliminarTildes($string);
}
protected static function llamarApi($ruta, $metodo){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $ruta,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $metodo,
		  CURLOPT_HTTPHEADER => array(
		       "cache-control: no-cache"
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	protected static function eliminarImagen($nombre, $carpeta){
		if(empty($carpeta)){
			if(!is_array($carpeta))
				$ruta = dirname(__FILE__).Self::RUTA . '/'. $nombre;

			else{
				foreach ($carpeta as $key => $value) {
					$ruta = dirname(__FILE__).Self::RUTA.'/'.$value;
				}
			} 
		unlink($ruta);
		}
		else
			{
				$ruta = dirname(__FILE__).Self::RUTA . '/'. $carpeta´ . '/'. $nombre;
			}
	}
	
	protected static function guardarImagen($file, $nombre, $carpeta){
		  $extencion = explode('.', $file['name'])[1];
		  //var_dump($extencion);die;
		  $nombrearchivo = $nombre .".". $extencion;
          $rutatemp = $file['tmp_name'];
          $ruta = dirname(__FILE__). Self::RUTA . $carpeta.'/'. $nombrearchivo;
         // var_dump($ruta);die();
          if (is_uploaded_file($rutatemp)) {
            if(copy($rutatemp, $ruta)== FALSE){//intento copiar la imagen del tmp al servidor
                return -1;
            }
            else 
            	return  'https://'.$_SERVER['HTTP_HOST'].'/public/imagenes/'.$carpeta.'/'.$nombrearchivo;
          }
    }

    protected static function obtenerDistancia($lat1, $long1, $lat2, $long2) {
	    $degtorad = 0.01745329;
	    $radtodeg = 57.29577951;

	    $dlong = ($long1 - $long2);
	    $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad));

	    $dd = acos($dvalue) * $radtodeg;

	    $miles = ($dd * 69.16);
	    $km = ($dd * 111.302);
	    $metros = $km * 1000;
	    //var_dump($metros);die();
	    $km = number_format($km, 2, '.', '');
	    $metros = number_format($metros, 2, '.', '');

	    return array("km"=>$km, "metros"=>$metros);
	}
	protected static function letraAleatoria(){
		$letras = "QWERTYUIOPASDFGHJHJKLMNBVCXZ";
		return substr($letras, rand(0, strlen($letras)-1),1);
	}
	protected static function numeroAleatorio(){
		$letras = "1234567890";
		return substr($letras, rand(0, strlen($letras)-1),1);
	}
	protected static function salvarTexto($string){
		$rta = "";
		$cadena = explode('""', $string);
		if(is_array($cadena)){
			foreach ($cadena as $key => $value) {
				if(empty($rta))
					$rta = $value;
				else
					$rta .= '\\"'. $value;
			}
		}
		else
			$rta= $cadena;
		return $rta;
	}
	protected static function restructurarFile($vector) { 
    $result = []; 
    foreach($vector as $key1 => $value1) 
    	     foreach($value1 as $key2 => $value2) 
            	$result[$key2][$key1] = $value2; 
     return $result;
    }
    protected static function buscarAparicion($s, $accept) {
	  $r = FALSE;
	  $t = 0;
	  $i = 0;
	  $accept_l = strlen($accept);

	  for ( ; $i < $accept_l ; $i++ )
	    if ( ($t = strpos($s, $accept{$i})) !== FALSE )
	      if ( ($r === FALSE) || ($t < $r) )
	        $r = $t;

	  return $v;
	}   

}
?>