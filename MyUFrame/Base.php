<?php 	
namespace admin\MyUFrame;
use \PDO;
/**
 * 
 */
class Base 
{
	CONST HOST= "localhost";
	CONST DB = "facturacion_admin";
	CONST USERNAME = "root";
	CONST PASSWORD = "";

	private static function getConnection(){
            $username = self::USERNAME;
            $password = self::PASSWORD;
            $host = self::HOST;
            $db = self::DB;
            $connection = new PDO("mysql:dbname=$db;host=$host;charset=utf8", $username, $password);
            return $connection;
    }
    protected static function ejecutarConsulta($sql, $args = []){
        $connection = Self::getConnection();
        $stmt = $connection->prepare($sql);
        if(!empty($args))
            $stmt->execute($args);
        else
            $stmt->execute();
        $rta = Self::procesarRespuesta($stmt);
        $insert = $connection->lastInsertId();
        //print_r($connection->errorInfo());
        if(substr($sql, 0,1) == 'I'){
            return $insert;
        }
        elseif(substr($sql, 0,1) == 'U')
            return $stmt->rowCount();
        else
            return $rta;
    }
    protected static function ultimoId($nombre){
    }
    private function getConnecion(){
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $host = self::HOST;
        $db = self::DB;
        $connection = new PDO("mysql:dbname=$db;host=$host", $username, $password);
        return $connection;
    }
    protected function query($sql, $args){
        $connection = $this->getConnecion();
        $stmt = $connection->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
    protected function mostrarQuery($query, $params)
    {
        $keys = [];
        $values = [];
        
        foreach ($params as $key=>$value)
        {
            if (is_string($key))
            {
                $keys[] = '/:'.$key.'/';
            }
            else
            {
                $keys[] = '/[?]/';
            }
            
            if(is_numeric($value))
            {
                $values[] = intval($value);
            }
            else
            {
                $values[] = '"'.$value .'"';
            }
        }
        
        $query = preg_replace($keys, $values, $query, 1, $count);
        return $query;
    }
    private static function procesarRespuesta($respuestaPDO){
        if(is_object($respuestaPDO)){    
            $rta = [];
            while ($fila = $respuestaPDO->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                array_push($rta, $fila);
            }
        }
            return $rta;
    }
}
?>