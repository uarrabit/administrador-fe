<?php   
namespace admin\MyUFrame;
class Respuesta extends Clase
{
  protected $codigo;
  protected $mensaje;
  
  public function __construct($codigo, $mensaje){
       $this->codigo = sprintf("%s",$codigo);
       if(!is_object($mensaje) && !is_array($mensaje))
        $this->mensaje = sprintf("%s",$mensaje);
      else
        $this->mensaje = $mensaje;
    }
    public function setCodigo( $value ){
        $this->codigo = sprintf("%s", $value);
    }
    public function setMensaje( $value ){
       $this->mensaje = $value;
    }
    public function getCodigo(){
        return $this->codigo;
    }
    public function getMensaje(){
        return $this->mensaje;
    }
    public function getJson(){
        $vars = get_object_vars($this);
        return Self::getArray($vars);
    }
}
?>