<?php 	
namespace admin\MyUFrame;
/**
 * 
 */
class Combo
{
	private $id;
	private $intancia;

	public function __construct($id, $intancia){
	   $this->id = sprintf("%s",$id);
	   $this->intancia = sprintf("%s",$intancia);
	}
    public function setId( $value ){
        $this->id = sprintf("%s", $value);
    }
    public function setIntancia( $value ){
        $this->intancia = sprintf("%s", $value);
    }
    public function getId(){
        return $this->id;
    }
    public function getIntancia(){
        return $this->intancia;
    }
    public function getJson(){
      return get_object_vars($this);
    }
}
?>