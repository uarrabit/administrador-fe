<?php 
namespace admin\MyUFrame;
//include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\autoload.inc.php");
/**
 * 
 */
class DB extends Base
{
	public static function vaciarBaseDatos(){
		$sql = "
		TRUNCATE `".Self::DB."`.`usuarios`;
		TRUNCATE `".Self::DB."`.`usuarios_fcm`;
		TRUNCATE `".Self::DB."`.`usuarios_hogares`;
		TRUNCATE `".Self::DB."`.`usuarios_perfiles`;
		TRUNCATE `".Self::DB."`.`mascotas_perros`;
		TRUNCATE `".Self::DB."`.`mascotas_mascotas`;
		TRUNCATE `".Self::DB."`.`mascotas_imagenes`;
		TRUNCATE `".Self::DB."`.`mascotas_estados`;
		TRUNCATE `".Self::DB."`.`mascotas_contactos`;
		TRUNCATE `".Self::DB."`.`mascotas_buscadas_contactos`;
		TRUNCATE `".Self::DB."`.`mascotas_buscadas`;
		TRUNCATE `".Self::DB."`.`hogares_membresias`;
		TRUNCATE `".Self::DB."`.`hogares_mascotas`;
		TRUNCATE `".Self::DB."`.`hogares`;
		TRUNCATE `".Self::DB."`.`usuarios_veterinarios`;
		TRUNCATE `".Self::DB."`.`usuarios_telefonos`;
		TRUNCATE `".Self::DB."`.`veterinarios_domicilios_atencion`;
		TRUNCATE `".Self::DB."`.`veterinarios_especialidades`;
		TRUNCATE `".Self::DB."`.`veterinarios_tiposMascotas`;
		TRUNCATE `".Self::DB."`.`usuarios_redessociales`;
		TRUNCATE `".Self::DB."`.`mercado_pago_transacciones`;
		TRUNCATE `".Self::DB."`.`mascotas_veterinarios_autorizados`;";
		$sql = explode(";", $sql);
		foreach ($sql as $key => $value) {
			Self::ejecutarConsulta($value);
		}
		Self::ejecutarConsulta("INSERT INTO `hogares`(`fechaAlta`,`hogar`) VALUES ('".date('Y-m-d H:i:s')."', 'sin asignar')");
		Self::ejecutarConsulta("INSERT INTO `hogares_membresias`(`fechaAlta`,fk_hogar, fechaHasta, fk_membresia) VALUES (CURRENT_TIMESTAMP, 1, '2030-12-31',1)");
	}
}

DB::vaciarBaseDatos();
?>