<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit0c753bf695a3ee8a201118b317865695
{
    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'PHPGit_' => 
            array (
                0 => __DIR__ . '/../..' . '/lib',
            ),
        ),
    );

    public static $classMap = array (
        'Cz\\Git\\GitException' => __DIR__ . '/..' . '/czproject/git-php/src/IGit.php',
        'Cz\\Git\\GitRepository' => __DIR__ . '/..' . '/czproject/git-php/src/GitRepository.php',
        'Cz\\Git\\IGit' => __DIR__ . '/..' . '/czproject/git-php/src/IGit.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInit0c753bf695a3ee8a201118b317865695::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit0c753bf695a3ee8a201118b317865695::$classMap;

        }, null, ClassLoader::class);
    }
}
