<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	use admin\code\usuarios\CtrlUsuario;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		 print( json_encode(CtrlUsuario::controlarMail($_GET['mail'])->getJson()));
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		$body = json_decode(file_get_contents("php://input"), true);
		print( json_encode(CtrlUsuario::login($body["mail"], $body['clave'])->getJson()));
	}	
	
?>