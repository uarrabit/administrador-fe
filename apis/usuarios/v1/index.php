<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	use admin\code\usuarios\CtrlUsuario;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		if(empty($_GET["id"]) && empty($_GET['mail']))
			print( json_encode(CtrlUsuario::usuario()->getJson() ) ) ;
		elseif(empty($_GET['mail']))
			print( json_encode(CtrlUsuario::leerUsuario($_GET["id"])->getJson() ) );
		else
			@ print( json_encode(CtrlUsuario::olvideMiClave($_GET['mail'])->getJson()));
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		 $body = json_decode(file_get_contents("php://input"), true);
		 print( json_encode(CtrlUsuario::login($_POST["mail"], $_POST['clave'])->getJson()));
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		$body = json_decode(file_get_contents("php://input"), true);
		print( json_encode(CtrlUsuario::actualizarUsuario($body["id"],$body["razon_social"], $body["nombre"], $body["apellido"], $body["cuit"], $body["fk_condicion_iva"], $body["fk_perfil"], $body["ruta"])->getJson()));
	}
	elseif($_SERVER["REQUEST_METHOD"] == "PATCH"){
		$body = json_decode(file_get_contents("php://input"), true);
		if(empty($body['tkn']))
			@ print( json_encode(CtrlUsuario::olvideMiClave($body["mail"])->getJson()));
		else
			print( json_encode(CtrlUsuario::blanqueoClave($body["tkn"], $body['clave'])->getJson()));
	}
?>