<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	use admin\code\usuarios\CtrlUsuario;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		 print( json_encode(CtrlUsuario::controlarMail($_GET['mail'])->getJson()));
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		$body = json_decode(file_get_contents("php://input"), true);
		if(empty($body['tkn']) && empty($_POST['mail']))
			@ print( json_encode(CtrlUsuario::olvideMiClave($body["mail"])->getJson()));
		elseif (!empty($_POST['mail'])) {
			 print( json_encode(CtrlUsuario::login($_POST["mail"], $_POST['clave'])->getJson()));
		}
		else
			print( json_encode(CtrlUsuario::blanqueoClave($body["tkn"], $body['clave'])->getJson()));
	}	
	
?>