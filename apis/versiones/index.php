<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\autoload.inc.php");
	use code\versiones\CtrlVersion;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		if(empty($_GET["id"]) && empty($_GET['version']))
			print( json_encode(CtrlVersion::version()->getJson() ) ) ;
		elseif(empty($_GET['version']))
			print( json_encode(CtrlVersion::leerVersion($_GET["id"])->getJson() ) );
		else
			print(json_encode(CtrlVersion::comprobarVersion($_GET['version'])->getJson()));
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		 $body = json_decode(file_get_contents("php://input"), true);
		 print( json_encode(CtrlVersion::altaVersion($body["version"], $body["compativilidad_minima"])->getJson()));
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		$body = json_decode(file_get_contents("php://input"), true);
		print( json_encode(CtrlVersion::actualizarVersion($body["id"],$body["version"], $body["compativilidad_minima"])->getJson()));
	}
?>