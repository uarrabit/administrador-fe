<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT");
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	use code\administradores\CtrlAdministrador;
	
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		if(empty($_GET["id"]))
			print( json_encode(CtrlAdministrador::administrador()->getJson() ) ) ;
		else
			print( json_encode(CtrlAdministrador::leerAdministrador($_GET["id"])->getJson() ) );
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		 $body = json_decode(file_get_contents("php://input"), true);
		 if(isset($body['nombre']) && !empty($body['nombre']))
		 	print( json_encode(CtrlAdministrador::altaAdministrador($body["nombre"], $body["apellido"], $body["mail"], $body["clave"])->getJson()));
		 else
		 	print( json_encode( CtrlAdministrador::login($body['mail'], $body['clave'])->getJson()));
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		$body = json_decode(file_get_contents("php://input"), true);
		if(!empty($body['claveNueva']))
			print( json_encode(CtrlAdministrador::actualizarClave($body["claveVieja"],$body["claveNueva"], $body["idAdmin"])->getJson()));
	}
?>