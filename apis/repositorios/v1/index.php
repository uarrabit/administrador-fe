<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	
	use code\repositorios\CtrlRepositorio;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		print( json_encode(CtrlRepositorio::todosLosRepos()->getJson() ) );
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
			print( json_encode(CtrlRepositorio::alctualizarBaseDatos()->getJson() ) ) ;
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		print( json_encode(CtrlRepositorio::pruebaActualizarRepos() ) ) ;
	}
?>