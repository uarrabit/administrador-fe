<?php
	include_once dirname(__FILE__).str_replace("\\", DIRECTORY_SEPARATOR,"\..\..\..\autoload.inc.php");
	use code\repositorios\v2\CtrlRepositorio;
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT");
	//$auth = apache_request_headers()["Token"];
	
	if($_SERVER["REQUEST_METHOD"] == "GET"){
		if(empty($_GET["id"]) && empty($_GET['idEstado']))
			print( json_encode(CtrlRepositorio::proximoRepo()->getJson() ) ) ;
		elseif(!empty($_GET['id']))
			print( json_encode(CtrlRepositorio::leerRepositorio($_GET["id"])->getJson() ) );
		else
			print( json_encode(CtrlRepositorio::repositorio($_GET['idEstado'])->getJson() ) ) ;
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST"){
		 $body = json_decode(file_get_contents("php://input"), true);
		 print( json_encode(CtrlRepositorio::altaRepositorio($body["ruta"])->getJson()));
	}	
	elseif($_SERVER["REQUEST_METHOD"] == "PUT"){
		$body = json_decode(file_get_contents("php://input"), true);
		print( json_encode(CtrlRepositorio::actualizarRepositorio($body["id"], $body["idEstado"])->getJson()));
	}
?>